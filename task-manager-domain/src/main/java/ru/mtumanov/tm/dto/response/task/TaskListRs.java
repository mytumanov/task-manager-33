package ru.mtumanov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultRs;
import ru.mtumanov.tm.model.Task;

import java.util.List;

@Getter
@NoArgsConstructor
public final class TaskListRs extends AbstractResultRs {

    @Nullable
    private List<Task> tasks;

    public TaskListRs(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

    public TaskListRs(@NotNull final Throwable err) {
        super(err);
    }

}