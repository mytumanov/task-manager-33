package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
public class ProjectStartByIdRq extends AbstractUserRq {

    @Nullable
    private String id;

    public ProjectStartByIdRq(@Nullable final String id) {
        this.id = id;
    }

}
