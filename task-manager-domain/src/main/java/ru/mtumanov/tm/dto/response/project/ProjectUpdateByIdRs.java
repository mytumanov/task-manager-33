package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectUpdateByIdRs extends AbstractProjectRs {

    public ProjectUpdateByIdRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectUpdateByIdRs(@Nullable final Throwable err) {
        super(err);
    }

}