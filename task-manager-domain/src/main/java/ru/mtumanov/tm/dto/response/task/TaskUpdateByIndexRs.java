package ru.mtumanov.tm.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

public final class TaskUpdateByIndexRs extends AbstractTaskRs {

    public TaskUpdateByIndexRs(@Nullable final Task task) {
        super(task);
    }

    public TaskUpdateByIndexRs(@NotNull final Throwable err) {
        super(err);
    }

}