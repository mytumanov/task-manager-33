package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
public class ProjectStartByIndexRq extends AbstractUserRq {

    @Nullable
    private Integer index;

    public ProjectStartByIndexRq(@Nullable final Integer index) {
        this.index = index;
    }

}
