package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
public class TaskStartByIdRq extends AbstractUserRq {

    @Nullable
    private String id;

    public TaskStartByIdRq(@Nullable final String id) {
        this.id = id;
    }

}
