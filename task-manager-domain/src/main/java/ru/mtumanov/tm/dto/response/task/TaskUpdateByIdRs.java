package ru.mtumanov.tm.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

public final class TaskUpdateByIdRs extends AbstractTaskRs {

    public TaskUpdateByIdRs(@Nullable final Task task) {
        super(task);
    }

    public TaskUpdateByIdRs(@NotNull final Throwable err) {
        super(err);
    }

}