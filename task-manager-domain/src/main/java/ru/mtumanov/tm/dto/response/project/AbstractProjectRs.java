package ru.mtumanov.tm.dto.response.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultRs;
import ru.mtumanov.tm.model.Project;

public abstract class AbstractProjectRs extends AbstractResultRs {

    @Nullable
    @Getter
    private Project project;

    protected AbstractProjectRs(@Nullable final Project project) {
        this.project = project;
    }

    protected AbstractProjectRs(@NotNull Throwable err) {
        super(err);
    }

}
