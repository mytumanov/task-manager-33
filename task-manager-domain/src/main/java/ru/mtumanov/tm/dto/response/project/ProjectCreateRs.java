package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectCreateRs extends AbstractProjectRs {

    public ProjectCreateRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectCreateRs(@NotNull final Throwable err) {
        super(err);
    }

}