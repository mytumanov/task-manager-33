package ru.mtumanov.tm.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

public final class TaskChangeStatusByIndexRs extends AbstractTaskRs {

    public TaskChangeStatusByIndexRs(@Nullable final Task task) {
        super(task);
    }

    public TaskChangeStatusByIndexRs(@NotNull final Throwable err) {
        super(err);
    }

}