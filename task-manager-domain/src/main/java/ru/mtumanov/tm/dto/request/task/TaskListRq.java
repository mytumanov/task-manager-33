package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;
import ru.mtumanov.tm.enumerated.TaskSort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRq extends AbstractUserRq {

    @Nullable
    private TaskSort sort;

    public TaskListRq(@Nullable final TaskSort sort) {
        this.sort = sort;
    }

}