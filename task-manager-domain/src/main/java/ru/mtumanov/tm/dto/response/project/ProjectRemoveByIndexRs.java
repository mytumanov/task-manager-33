package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectRemoveByIndexRs extends AbstractProjectRs {

    public ProjectRemoveByIndexRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectRemoveByIndexRs(@Nullable final Throwable err) {
        super(err);
    }

}