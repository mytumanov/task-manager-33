package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
public class TaskCompleteByIdRq extends AbstractUserRq {

    @Nullable
    private String id;

    public TaskCompleteByIdRq(@Nullable final String id) {
        this.id = id;
    }

}
