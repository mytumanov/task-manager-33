package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public class ProjectStartByIdRs extends AbstractProjectRs {

    public ProjectStartByIdRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectStartByIdRs(@Nullable final Throwable err) {
        super(err);
    }

}
