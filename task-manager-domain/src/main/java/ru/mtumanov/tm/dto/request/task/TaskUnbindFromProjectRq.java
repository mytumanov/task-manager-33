package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUnbindFromProjectRq extends AbstractUserRq {

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    public TaskUnbindFromProjectRq(@Nullable final String taskId, @Nullable final String projectId) {
        this.taskId = taskId;
        this.projectId = projectId;
    }

}