package ru.mtumanov.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.response.AbstractResultRs;

@NoArgsConstructor
public final class UserLoginRs extends AbstractResultRs {

    public UserLoginRs(@NotNull Throwable throwable) {
        super(throwable);
    }

}