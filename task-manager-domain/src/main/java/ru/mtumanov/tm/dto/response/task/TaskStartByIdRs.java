package ru.mtumanov.tm.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

public class TaskStartByIdRs extends AbstractTaskRs {

    public TaskStartByIdRs(@Nullable final Task task) {
        super(task);
    }

    public TaskStartByIdRs(@NotNull final Throwable err) {
        super(err);
    }

}
