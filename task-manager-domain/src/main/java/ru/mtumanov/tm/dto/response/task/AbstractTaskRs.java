package ru.mtumanov.tm.dto.response.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultRs;
import ru.mtumanov.tm.model.Task;

@Getter
public abstract class AbstractTaskRs extends AbstractResultRs {

    @Nullable
    private Task task;

    protected AbstractTaskRs(@Nullable final Task task) {
        this.task = task;
    }

    protected AbstractTaskRs(@NotNull final Throwable err) {
        super(err);
    }

}