package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectChangeStatusByIdRs extends AbstractProjectRs {

    public ProjectChangeStatusByIdRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectChangeStatusByIdRs(@NotNull final Throwable err) {
        super(err);
    }

}