package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowByIdRq extends AbstractUserRq {

    @Nullable
    private String id;

    public ProjectShowByIdRq(@Nullable final String id) {
        this.id = id;
    }

}