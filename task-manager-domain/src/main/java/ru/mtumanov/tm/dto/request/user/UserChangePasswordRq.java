package ru.mtumanov.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordRq extends AbstractUserRq {

    @Nullable
    private String password;

    public UserChangePasswordRq(@Nullable final String password) {
        this.password = password;
    }

}