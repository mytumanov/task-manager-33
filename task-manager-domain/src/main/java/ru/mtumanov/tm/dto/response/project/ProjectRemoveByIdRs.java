package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectRemoveByIdRs extends AbstractProjectRs {

    public ProjectRemoveByIdRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectRemoveByIdRs(@Nullable final Throwable err) {
        super(err);
    }

}