package ru.mtumanov.tm.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

public final class TaskShowByIdRs extends AbstractTaskRs {

    public TaskShowByIdRs(@Nullable final Task task) {
        super(task);
    }

    public TaskShowByIdRs(@NotNull final Throwable err) {
        super(err);
    }

}