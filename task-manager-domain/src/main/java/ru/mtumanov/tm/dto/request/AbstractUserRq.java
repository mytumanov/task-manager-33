package ru.mtumanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractUserRq extends AbstractRq {

    @Nullable
    protected String userId;

}
