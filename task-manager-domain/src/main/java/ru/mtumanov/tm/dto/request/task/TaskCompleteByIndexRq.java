package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
public class TaskCompleteByIndexRq extends AbstractUserRq {

    @Nullable
    private Integer index;

    public TaskCompleteByIndexRq(@Nullable final Integer index) {
        this.index = index;
    }

}
