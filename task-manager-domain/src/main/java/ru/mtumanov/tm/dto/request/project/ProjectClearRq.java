package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.Setter;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
public final class ProjectClearRq extends AbstractUserRq {

}