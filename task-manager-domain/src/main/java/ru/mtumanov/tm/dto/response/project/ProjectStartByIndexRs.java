package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public class ProjectStartByIndexRs extends AbstractProjectRs {

    public ProjectStartByIndexRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectStartByIndexRs(@NotNull final Throwable err) {
        super(err);
    }

}
