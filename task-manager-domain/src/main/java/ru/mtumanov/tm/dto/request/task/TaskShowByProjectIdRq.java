package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
public class TaskShowByProjectIdRq extends AbstractUserRq {

    @Nullable
    private String projectId;

    public TaskShowByProjectIdRq(@Nullable final String projectId) {
        this.projectId = projectId;
    }

}
