package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectUpdateByIndexRs extends AbstractProjectRs {

    public ProjectUpdateByIndexRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectUpdateByIndexRs(@Nullable final Throwable err) {
        super(err);
    }

}