package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectShowByIndexRs extends AbstractProjectRs {

    public ProjectShowByIndexRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectShowByIndexRs(@Nullable final Throwable err) {
        super(err);
    }

}
