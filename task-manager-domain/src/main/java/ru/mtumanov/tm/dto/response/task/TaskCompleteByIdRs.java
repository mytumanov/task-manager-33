package ru.mtumanov.tm.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

public class TaskCompleteByIdRs extends AbstractTaskRs {

    public TaskCompleteByIdRs(@Nullable final Task task) {
        super(task);
    }

    public TaskCompleteByIdRs(@NotNull final Throwable err) {
        super(err);
    }

}
