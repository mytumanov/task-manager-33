package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.UserLoginRq;
import ru.mtumanov.tm.dto.request.user.UserLogoutRq;
import ru.mtumanov.tm.dto.response.user.UserLoginRs;
import ru.mtumanov.tm.dto.response.user.UserLogoutRs;

public interface IAuthEndpoint {

    @NotNull
    UserLoginRs login(@NotNull UserLoginRq request);

    @NotNull
    UserLogoutRs logout(@NotNull UserLogoutRq request);

}
