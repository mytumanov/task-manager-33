package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.*;
import ru.mtumanov.tm.dto.response.user.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.net.MalformedURLException;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() throws MalformedURLException {
        return IEndpoint.newInstance(NAME, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserChangePasswordRs userChangePassword(@NotNull UserChangePasswordRq request);

    @NotNull
    @WebMethod
    UserLockRs userLock(@NotNull UserLockRq request);

    @NotNull
    @WebMethod
    UserRegistryRs userRegistry(@NotNull UserRegistryRq request);

    @NotNull
    @WebMethod
    UserRemoveRs userRemove(@NotNull UserRemoveRq request);

    @NotNull
    @WebMethod
    UserUnlockRs userUnlock(@NotNull UserUnlockRq request);

    @NotNull
    @WebMethod
    UserProfileRs userProfile(@NotNull UserProfileRq request);

    @NotNull
    @WebMethod
    UserUpdateRs userUpdate(@NotNull UserUpdateRq request);

}
