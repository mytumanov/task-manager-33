package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.*;
import ru.mtumanov.tm.dto.response.data.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.MalformedURLException;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() throws MalformedURLException {
        return IEndpoint.newInstance(NAME, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBase64LoadRs loadDataBase64(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBase64LoadRq request);

    @NotNull
    @WebMethod
    DataBase64SaveRs saveDataBase64(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBase64SaveRq request);

    @NotNull
    @WebMethod
    DataBinaryLoadRs loadDataBinary(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBinaryLoadRq request);

    @NotNull
    @WebMethod
    DataBinarySaveRs saveDataBinary(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBinarySaveRq request);

    @NotNull
    @WebMethod
    DataJsonLoadFasterXmlRs loadDataJsonFasterXml(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJsonLoadFasterXmlRq request);

    @NotNull
    @WebMethod
    DataJsonSaveFasterXmlRs saveDataJsonFasterXml(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJsonSaveFasterXmlRq request);

    @NotNull
    @WebMethod
    DataJsonLoadJaxbRs loadDataJsonJaxb(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJsonLoadJaxbRq request);

    @NotNull
    @WebMethod
    DataJsonSaveJaxbRs saveDataJsonJaxb(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJsonSaveJaxbRq request);

    @NotNull
    @WebMethod
    DataXmlLoadFasterXmlRs loadDataXmlFasterXml(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataXmlLoadFasterXmlRq request);

    @NotNull
    @WebMethod
    DataXmlSaveFasterXmlRs saveDataXmlFasterXml(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataXmlSaveFasterXmlRq request);

    @NotNull
    @WebMethod
    DataXmlLoadJaxbRs loadDataXmlJaxb(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataXmlLoadJaxbRq request);

    @NotNull
    @WebMethod
    DataXmlSaveJaxbRs saveDataXmlJaxb(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataXmlSaveJaxbRq request);

    @NotNull
    @WebMethod
    DataYamlLoadFasterXmlRs loadDataYamlFasterXml(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataYamlLoadFasterXmlRq request);

    @NotNull
    @WebMethod
    DataYamlSaveFasterXmlRs saveDataYamlFasterXml(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataYamlSaveFasterXmlRq request);

}
