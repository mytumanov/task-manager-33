package ru.mtumanov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.api.service.ICommandService;
import ru.mtumanov.tm.api.service.ILoggerService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.client.*;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.system.ArgumentNotSupportedException;
import ru.mtumanov.tm.exception.system.CommandNotSupportedException;
import ru.mtumanov.tm.repository.CommandRepository;
import ru.mtumanov.tm.service.CommandService;
import ru.mtumanov.tm.service.LoggerService;
import ru.mtumanov.tm.service.PropertyService;
import ru.mtumanov.tm.util.SystemUtil;
import ru.mtumanov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PROJECT_COMMANDS = "ru.mtumanov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    @Getter
    private final SystemEndpointClient systemEndpoint = new SystemEndpointClient();

    @NotNull
    @Getter
    private final DomainEndpointClient domainEndpoint = new DomainEndpointClient();

    @NotNull
    @Getter
    private final ProjectEndpointClient projectEndpoint = new ProjectEndpointClient();

    @NotNull
    @Getter
    private final TaskEndpointClient taskEndpoint = new TaskEndpointClient();

    @NotNull
    @Getter
    private final UserEndpointClient userEndpoint = new UserEndpointClient();

    @NotNull
    @Getter
    private final AuthEndpointClient authEndpoint = new AuthEndpointClient();


    {
        @NotNull final Reflections reflections = new Reflections(PROJECT_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes)
            registry(clazz);
    }

    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(fileName), pid.getBytes());
        } catch (@NotNull final IOException e) {
            loggerService.error(e);
        }
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String[] args) {
        init();

        processArguments(args);
        while (Thread.currentThread().isAlive()) {
            System.out.println("ENTER COMMAND:");
            try {
                @NotNull final String cmd = TerminalUtil.nextLine();
                processCommand(cmd);
                System.out.println("OK");
                loggerService.command(cmd);
            } catch (final Exception e) {
                System.out.println("FAIL");
                loggerService.error(e);
            }
        }
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0 || args[0] == null)
            return;
        try {
            processArgument(args[0]);
            System.exit(0);
        } catch (final AbstractException e) {
            System.out.println(e.getMessage());
        }
    }

    private void processArgument(@NotNull String arg) throws AbstractException {
        if (arg.isEmpty())
            return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null)
            throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@NotNull final String command) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        try {
            @NotNull final AbstractCommand command = clazz.getDeclaredConstructor().newInstance();
            registry(command);
        } catch (ReflectiveOperationException e) {
            loggerService.error(e);
        }
    }

    private void init() {
        loggerService.info("** WELCOME TO TASK_MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        initPID();
        fileScanner.init();
    }

    private void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

}
