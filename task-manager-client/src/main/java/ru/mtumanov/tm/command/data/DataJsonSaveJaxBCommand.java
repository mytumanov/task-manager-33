package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataJsonSaveJaxbRq;
import ru.mtumanov.tm.dto.response.data.DataJsonSaveJaxbRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to json file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-json-jaxb";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveJaxbRs response = getDomainEndpoint().saveDataJsonJaxb(new DataJsonSaveJaxbRq());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
