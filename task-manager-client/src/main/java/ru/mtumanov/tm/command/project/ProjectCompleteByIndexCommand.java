package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.project.ProjectCompleteByIndexRq;
import ru.mtumanov.tm.dto.response.project.ProjectCompleteByIndexRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Complete project by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-complete-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectCompleteByIndexRq request = new ProjectCompleteByIndexRq(index);
        @NotNull final ProjectCompleteByIndexRs response = getProjectEndpoint().projectCompleteByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
