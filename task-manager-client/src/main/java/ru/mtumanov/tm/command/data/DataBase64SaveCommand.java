package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataBase64SaveRq;
import ru.mtumanov.tm.dto.response.data.DataBase64SaveRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-base64";

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to base64 file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE BASE64]");
        @NotNull final DataBase64SaveRs response = getDomainEndpoint().saveDataBase64(new DataBase64SaveRq());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
