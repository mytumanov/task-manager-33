package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.user.UserLogoutRq;
import ru.mtumanov.tm.dto.response.user.UserLogoutRs;
import ru.mtumanov.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "logout current user";
    }

    @Override
    @NotNull
    public String getName() {
        return "logout";
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRq request = new UserLogoutRq();
        @NotNull final UserLogoutRs response = getAuthEndpoint().logout(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
