package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataBase64LoadRq;
import ru.mtumanov.tm.dto.response.data.DataBase64LoadRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from base64 file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD BASE64]");
        @NotNull final DataBase64LoadRs response = getDomainEndpoint().loadDataBase64(new DataBase64LoadRq());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
