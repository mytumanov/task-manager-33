package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.ISystemEndpoint;
import ru.mtumanov.tm.dto.request.server.ServerAboutRq;
import ru.mtumanov.tm.dto.request.server.ServerVersionRq;
import ru.mtumanov.tm.dto.response.server.ServerAboutRs;
import ru.mtumanov.tm.dto.response.server.ServerVersionRs;

public class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @Override
    @NotNull
    @SneakyThrows
    public ServerAboutRs getAbout(@NotNull final ServerAboutRq serverAboutRequest) {
        return call(serverAboutRequest, ServerAboutRs.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public ServerVersionRs getVersion(@NotNull ServerVersionRq serverVersionRequest) {
        return call(serverVersionRequest, ServerVersionRs.class);
    }

}
