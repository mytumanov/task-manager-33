package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IAuthEndpoint;
import ru.mtumanov.tm.dto.request.user.UserLoginRq;
import ru.mtumanov.tm.dto.request.user.UserLogoutRq;
import ru.mtumanov.tm.dto.response.user.UserLoginRs;
import ru.mtumanov.tm.dto.response.user.UserLogoutRs;

public class AuthEndpointClient extends AbstractEndpoint implements IAuthEndpoint {

    @Override
    @SneakyThrows
    @NotNull
    public UserLoginRs login(@NotNull UserLoginRq request) {
        return call(request, UserLoginRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserLogoutRs logout(@NotNull UserLogoutRq request) {
        return call(request, UserLogoutRs.class);
    }

}
