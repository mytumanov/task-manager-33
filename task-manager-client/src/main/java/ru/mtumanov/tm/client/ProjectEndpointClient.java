package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IProjectEndpoint;
import ru.mtumanov.tm.dto.request.project.*;
import ru.mtumanov.tm.dto.response.project.*;

public class ProjectEndpointClient extends AbstractEndpoint implements IProjectEndpoint {

    @Override
    @SneakyThrows
    @NotNull
    public ProjectChangeStatusByIdRs projectChangeStatusById(
            @NotNull ProjectChangeStatusByIdRq request
    ) {
        return call(request, ProjectChangeStatusByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectChangeStatusByIndexRs projectChangeStatusByIndex(
            @NotNull ProjectChangeStatusByIndexRq request
    ) {
        return call(request, ProjectChangeStatusByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectClearRs projectClear(@NotNull ProjectClearRq request) {
        return call(request, ProjectClearRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectCompleteByIdRs projectCompleteById(@NotNull ProjectCompleteByIdRq request) {
        return call(request, ProjectCompleteByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectCompleteByIndexRs projectCompleteByIndex(
            @NotNull ProjectCompleteByIndexRq request) {
        return call(request, ProjectCompleteByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectCreateRs projectCreate(@NotNull ProjectCreateRq request) {
        return call(request, ProjectCreateRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectListRs projectList(@NotNull ProjectListRq request) {
        return call(request, ProjectListRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectRemoveByIdRs projectRemoveById(@NotNull ProjectRemoveByIdRq request) {
        return call(request, ProjectRemoveByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectRemoveByIndexRs projectRemoveByIndex(@NotNull ProjectRemoveByIndexRq request) {
        return call(request, ProjectRemoveByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectShowByIdRs projectShowById(@NotNull ProjectShowByIdRq request) {
        return call(request, ProjectShowByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectShowByIndexRs projectShowByIndex(@NotNull ProjectShowByIndexRq request) {
        return call(request, ProjectShowByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectStartByIdRs projectStartById(@NotNull ProjectStartByIdRq request) {
        return call(request, ProjectStartByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectStartByIndexRs projectStartByIndex(@NotNull ProjectStartByIndexRq request) {
        return call(request, ProjectStartByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectUpdateByIdRs projectUpdateById(@NotNull ProjectUpdateByIdRq request) {
        return call(request, ProjectUpdateByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectUpdateByIndexRs projectUpdateByIndex(@NotNull ProjectUpdateByIndexRq request) {
        return call(request, ProjectUpdateByIndexRs.class);
    }

}
