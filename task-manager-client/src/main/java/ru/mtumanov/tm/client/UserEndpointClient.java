package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IUserEndpoint;
import ru.mtumanov.tm.dto.request.user.*;
import ru.mtumanov.tm.dto.response.user.*;

public class UserEndpointClient extends AbstractEndpoint implements IUserEndpoint {

    @Override
    @SneakyThrows
    @NotNull
    public UserChangePasswordRs userChangePassword(@NotNull UserChangePasswordRq request) {
        return call(request, UserChangePasswordRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserLockRs userLock(@NotNull UserLockRq request) {
        return call(request, UserLockRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserRegistryRs userRegistry(@NotNull UserRegistryRq request) {
        return call(request, UserRegistryRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserRemoveRs userRemove(@NotNull UserRemoveRq request) {
        return call(request, UserRemoveRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserUnlockRs userUnlock(@NotNull UserUnlockRq request) {
        return call(request, UserUnlockRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserProfileRs userProfile(@NotNull UserProfileRq request) {
        return call(request, UserProfileRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserUpdateRs userUpdate(@NotNull UserUpdateRq request) {
        return call(request, UserUpdateRs.class);
    }

}
