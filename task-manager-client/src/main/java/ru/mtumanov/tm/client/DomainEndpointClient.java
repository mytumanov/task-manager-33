package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IDomainEndpoint;
import ru.mtumanov.tm.dto.request.data.*;
import ru.mtumanov.tm.dto.response.data.*;

public class DomainEndpointClient extends AbstractEndpoint implements IDomainEndpoint {

    @Override
    @SneakyThrows
    @NotNull
    public DataBase64LoadRs loadDataBase64(@NotNull DataBase64LoadRq request) {
        return call(request, DataBase64LoadRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataBase64SaveRs saveDataBase64(@NotNull DataBase64SaveRq request) {
        return call(request, DataBase64SaveRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataBinaryLoadRs loadDataBinary(@NotNull DataBinaryLoadRq request) {
        return call(request, DataBinaryLoadRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataBinarySaveRs saveDataBinary(@NotNull DataBinarySaveRq request) {
        return call(request, DataBinarySaveRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataJsonLoadFasterXmlRs loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRq request) {
        return call(request, DataJsonLoadFasterXmlRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataJsonSaveFasterXmlRs saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRq request) {
        return call(request, DataJsonSaveFasterXmlRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataJsonLoadJaxbRs loadDataJsonJaxb(@NotNull DataJsonLoadJaxbRq request) {
        return call(request, DataJsonLoadJaxbRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataJsonSaveJaxbRs saveDataJsonJaxb(@NotNull DataJsonSaveJaxbRq request) {
        return call(request, DataJsonSaveJaxbRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataXmlLoadFasterXmlRs loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRq request) {
        return call(request, DataXmlLoadFasterXmlRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataXmlSaveFasterXmlRs saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRq request) {
        return call(request, DataXmlSaveFasterXmlRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataXmlLoadJaxbRs loadDataXmlJaxb(@NotNull DataXmlLoadJaxbRq request) {
        return call(request, DataXmlLoadJaxbRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataXmlSaveJaxbRs saveDataXmlJaxb(@NotNull DataXmlSaveJaxbRq request) {
        return call(request, DataXmlSaveJaxbRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataYamlLoadFasterXmlRs loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRq request) {
        return call(request, DataYamlLoadFasterXmlRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataYamlSaveFasterXmlRs saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRq request) {
        return call(request, DataYamlSaveFasterXmlRs.class);
    }

}
