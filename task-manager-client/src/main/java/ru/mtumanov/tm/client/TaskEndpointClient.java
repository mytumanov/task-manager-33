package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.ITaskEndpoint;
import ru.mtumanov.tm.dto.request.task.*;
import ru.mtumanov.tm.dto.response.task.*;

public class TaskEndpointClient extends AbstractEndpoint implements ITaskEndpoint {

    @Override
    @SneakyThrows
    @NotNull
    public TaskChangeStatusByIdRs taskChangeStatusById(@NotNull TaskChangeStatusByIdRq request) {
        return call(request, TaskChangeStatusByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskChangeStatusByIndexRs taskChangeStatusByIndex(
            @NotNull TaskChangeStatusByIndexRq request
    ) {
        return call(request, TaskChangeStatusByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskClearRs taskClear(@NotNull TaskClearRq request) {
        return call(request, TaskClearRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskCompleteByIdRs taskCompleteById(@NotNull TaskCompleteByIdRq request) {
        return call(request, TaskCompleteByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskCompleteByIndexRs taskCompleteByIndex(@NotNull TaskCompleteByIndexRq request) {
        return call(request, TaskCompleteByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskCreateRs taskCreate(@NotNull TaskCreateRq request) {
        return call(request, TaskCreateRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskListRs taskList(@NotNull TaskListRq request) {
        return call(request, TaskListRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskRemoveByIdRs taskRemoveById(@NotNull TaskRemoveByIdRq request) {
        return call(request, TaskRemoveByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskRemoveByIndexRs taskRemoveByIndex(@NotNull TaskRemoveByIndexRq request) {
        return call(request, TaskRemoveByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskShowByIdRs taskShowById(@NotNull TaskShowByIdRq request) {
        return call(request, TaskShowByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskShowByIndexRs taskShowByIndex(@NotNull TaskShowByIndexRq request) {
        return call(request, TaskShowByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskShowByProjectIdRs taskShowByProjectId(@NotNull TaskShowByProjectIdRq request) {
        return call(request, TaskShowByProjectIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskStartByIdRs taskStartById(@NotNull TaskStartByIdRq request) {
        return call(request, TaskStartByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskStartByIndexRs taskStartByIndex(@NotNull TaskStartByIndexRq request) {
        return call(request, TaskStartByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskUpdateByIdRs taskUpdateById(@NotNull TaskUpdateByIdRq request) {
        return call(request, TaskUpdateByIdRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskUpdateByIndexRs taskUpdateByIndex(@NotNull TaskUpdateByIndexRq request) {
        return call(request, TaskUpdateByIndexRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskBindToProjectRs taskBindToProject(@NotNull TaskBindToProjectRq request) {
        return call(request, TaskBindToProjectRs.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskUnbindFromProjectRs taskUnbindFromProject(@NotNull TaskUnbindFromProjectRq request) {
        return call(request, TaskUnbindFromProjectRs.class);
    }

}
