package ru.mtumanov.tm.api.repository;

import ru.mtumanov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}
