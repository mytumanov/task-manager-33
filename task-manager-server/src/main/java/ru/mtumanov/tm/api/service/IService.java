package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    List<M> findAll();

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @NotNull
    M findOneById(@NotNull String id) throws AbstractException;

    @NotNull
    M findOneByIndex(@NotNull Integer index) throws AbstractException;

    @NotNull
    M remove(@NotNull M model);

    @NotNull
    M removeById(@NotNull String id) throws AbstractException;

    @NotNull
    M removeByIndex(@NotNull Integer index) throws AbstractException;

    void clear();

}
