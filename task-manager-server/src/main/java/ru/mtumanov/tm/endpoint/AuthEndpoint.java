package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IAuthEndpoint;
import ru.mtumanov.tm.dto.request.user.UserLoginRq;
import ru.mtumanov.tm.dto.request.user.UserLogoutRq;
import ru.mtumanov.tm.dto.response.user.UserLoginRs;
import ru.mtumanov.tm.dto.response.user.UserLogoutRs;

public class AuthEndpoint implements IAuthEndpoint {

    @Override
    @NotNull
    public UserLoginRs login(@NotNull UserLoginRq request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    @NotNull
    public UserLogoutRs logout(@NotNull UserLogoutRq request) {
        // TODO Auto-generated method stub
        return null;
    }

}
