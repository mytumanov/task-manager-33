package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.endpoint.ITaskEndpoint;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.request.task.*;
import ru.mtumanov.tm.dto.response.task.*;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.enumerated.TaskSort;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.mtumanov.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskChangeStatusByIdRs taskChangeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final Status status = request.getStatus();
            @NotNull final Task task = getServiceLocator().getTaskService().changeTaskStatusById(userId, id, status);
            return new TaskChangeStatusByIdRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskChangeStatusByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskChangeStatusByIndexRs taskChangeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIndexRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @Nullable final Status status = request.getStatus();
            @NotNull final Task task = getServiceLocator().getTaskService().changeTaskStatusByIndex(userId, index, status);
            return new TaskChangeStatusByIndexRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskChangeStatusByIndexRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskClearRs taskClear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            getServiceLocator().getTaskService().clear(userId);
            return new TaskClearRs();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskClearRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskCompleteByIdRs taskCompleteById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
            return new TaskCompleteByIdRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCompleteByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskCompleteByIndexRs taskCompleteByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIndexRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
            return new TaskCompleteByIndexRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCompleteByIndexRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskCreateRs taskCreate(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Task task = getServiceLocator().getTaskService().create(userId, name, description);
            return new TaskCreateRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCreateRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskListRs taskList(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final TaskSort sort = request.getSort();
            Comparator<Task> comparator = null;
            if (sort != null)
                comparator = sort.getComparator();
            @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAll(userId, comparator);
            return new TaskListRs(tasks);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskListRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskRemoveByIdRs taskRemoveById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().removeById(userId, id);
            return new TaskRemoveByIdRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskRemoveByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskRemoveByIndexRs taskRemoveByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIndexRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().removeByIndex(userId, index);
            return new TaskRemoveByIndexRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskRemoveByIndexRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskShowByIdRs taskShowById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().findOneById(userId, id);
            return new TaskShowByIdRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskShowByIndexRs taskShowByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIndexRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().findOneByIndex(userId, index);
            return new TaskShowByIndexRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByIndexRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskStartByIdRs taskStartById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
            return new TaskStartByIdRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskStartByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskStartByIndexRs taskStartByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIndexRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
            return new TaskStartByIndexRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskStartByIndexRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUpdateByIdRs taskUpdateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Task task = getServiceLocator().getTaskService().updateById(userId, id, name, description);
            return new TaskUpdateByIdRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUpdateByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUpdateByIndexRs taskUpdateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIndexRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Task task = getServiceLocator().getTaskService().updateByIndex(userId, index, name, description);
            return new TaskUpdateByIndexRs(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUpdateByIndexRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskBindToProjectRs taskBindToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @Nullable final String taskId = request.getTaskId();
            getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
            return new TaskBindToProjectRs();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskBindToProjectRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUnbindFromProjectRs taskUnbindFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @Nullable final String taskId = request.getTaskId();
            getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
            return new TaskUnbindFromProjectRs();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUnbindFromProjectRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskShowByProjectIdRs taskShowByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByProjectIdRq request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
            return new TaskShowByProjectIdRs(tasks);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByProjectIdRs(e);
        }
    }

}
