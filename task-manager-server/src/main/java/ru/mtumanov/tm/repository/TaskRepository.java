package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return models.stream()
                .filter(m -> projectId.equals(m.getProjectId()) && userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

}
